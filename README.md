# React-Formik

#### install : `yarn add formik` or `npm i formik`

#### Created by Vũ Anh Tuấn 14/09/2020

##### What?

- `Formik` is a small library that helps you deal with form in `React` (--> `Formik` là một thư viện nhỏ giúp bạn xử lí form trong `React` )

##### Why ?

- Managing form data
- Form submission
- Form validation and displaying error messages
- Formik helps you deal with forms in a scalable, performant and easy way (
  Formik giúp bạn xử lý các biểu mẫu theo cách có thể mở rộng, hiệu quả và dễ dàng )

### 01. Simple Form

Xem tại < OldYoutubeForm /> component
Ta xét 1 form sau :

```php
 <div>
      <form>
        <label htmlFor="name">Name</label>
        <input type="text" id="name" name="name" />

        <label htmlFor="email">E-mail</label>
        <input type="email" id="email" name="email" />

        <label htmlFor="channel">Channel</label>
        <input type="text" id="channel" name="channel" />

        <button>Submit</button>
      </form>
    </div>
```

---

### 02. useFormik Hook

```php
import { useFormik } from "formik";
//using :
const formik = useFormik({});
// => useFormik() trả về  1 đối tượng chứa nhiều thuộc tính và phương thức hữu ích (đối tương này là formik)
```

---

### 03. Managing Form State (Quản lí state của form)

- Managing the form state
- Handling form submission
- Validation and error messages

> 1. Managing the form state

```php
import React from "react";
import { useFormik } from "formik";
function YoutubeForm() {
  const formik = useFormik({
    // tạo giá trị mặc định cho form
    initialValues: {  // initialValues là của formik tự định nghĩa cho mình,, biến initialValues không phải là do mình đặt tên
      name: "",
      email: "",
      channel: "",
    },
  });
  console.log("Form value", formik.values);
  // => Form value {name: "", email: "", channel: ""}
  return (
    <div>
      <form>
        <label htmlFor="name">Name</label>
        <input
          type="text"
          id="name"
          name="name"
          onChange={formik.handleChange}  // bắt sự thay đổi của ô input bằng formik, Lưu ý: handleChange là của formik, không phải mình định nghĩa, nó là có sẵn của formik rồi nhé !!
          value={formik.values.name}  // => log(formik.values) -->  {name: "", email: "", channel: ""} , dân chơi nhìn phát biết ngay,kiểu giống value={state.name}
        />
         // Tương tự với 2 thằng input ở dưới
        <label htmlFor="email">E-mail</label>
        <input
          type="email"
          id="email"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />

        <label htmlFor="channel">Channel</label>
        <input
          type="text"
          id="channel"
          name="channel"
          onChange={formik.handleChange}
          value={formik.values.channel}
        />

        <button>Submit</button>
      </form>
    </div>
  );
}

export default YoutubeForm;

```

> 2. Handling form submission

```php
import React from "react";
import { useFormik } from "formik";
function YoutubeForm() {
  const formik = useFormik({
    initialValues: {
      name: "Vu Anh Tuan",  // giá trị mặc định của form, khi hiển thị thì 'Vu Anh Tuan' mặc định được bắn vào ô input
      email: "",
      channel: "",
    },
    // submit form dựa vào API của thằng formik, cái này là của formik, không phải mình tự định nghĩa
    onSubmit: function (values) {
      console.log("Form data", values);  // ==>
    },
  });
  // console.log("Form value", formik.values);
  // => Form value {name: "", email: "", channel: ""}
  return (
    <div>
      <form onSubmit={formik.handleSubmit}> // kích hoạt sự kiện onSubmit, sử  dụng API của formik là `formik.handleSubmit`, không phải mình tự định nghĩa !
        <label htmlFor="name">Name</label>
        <input
          type="text"
          id="name"
          name="name"
          onChange={formik.handleChange}
          value={formik.values.name}
        />

        <label htmlFor="email">E-mail</label>
        <input
          type="email"
          id="email"
          name="email"
          onChange={formik.handleChange}
          value={formik.values.email}
        />

        <label htmlFor="channel">Channel</label>
        <input
          type="text"
          id="channel"
          name="channel"
          onChange={formik.handleChange}
          value={formik.values.channel}
        />

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default YoutubeForm;

```

- **Kết quả console.log sau khi submit**

`Form data {name: "Vu Anh Tuan", email: "vuanhtuan743@gmail.com", channel: "react-formik"}`

#### Vậy để submit được form , đầu tiên phải gọi sự kiện `onSubmit` tại `form` với cú pháp : `onSubmit={formik.handleSubmit}`, sau đó tại `formik` object gọi `onSubmit` như là 1 method object !

> 3.Validation and error messages

```php
import React from "react";
import { useFormik } from "formik";

// CÓ THỂ VIẾT TÁCH RA THÀNH TỪNG  CÁI NHỎ (initialValues, onSubmit, validate)
// Tạo giá trị khởi tạo ban đầu cho input
const initialValues = {
  name: "Vu Anh Tuan",
  email: "",
  channel: "",
};

// hàm thực hiện submit form
// created by vatuan 15/09/2020
const onSubmit = (values) => {
  console.log("Form data", values);
};

// hàm thực hiện validate form
// created by vatuan 15/09/2020
const validate = (values) => {
  // values.name  values.email values.channel
  // errors.name errors.email errors.channel
  // errors.name = 'This field is required'
  // điều kiện đầu tiên của validate form là phải tạo ra 1 object sau đó return về object đó, trong ví dụ, object này chính là 'errors'
  let errors = {};
  if (!values.name) {
    errors.name = "Required";
  }
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email format";
  }
  if (!values.channel) {
    errors.channel = "Required";
  }
  return errors;
};

function YoutubeForm() {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate,
  });
  // console.log("Form value", formik.values);
  // => Form value {name: "", email: "", channel: ""}

  console.log("Form eror : ", formik.errors);
  // =>Form eror : {email: "Required", channel: "Required"}
  // Như vậy có thế lấy ra giá error-message bằng cách : formik.errors.email , kết quả trả về  sẽ là : 'Required'
  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
          />
          // Hiển thị thông báo validate tại đây !
          {formik.errors.name ? (
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="email">E-mail</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />

          // Hiển thị thông báo validate tại đây !
          {formik.errors.email ? (
            <div className="error">{formik.errors.email}</div>
          ) : null}
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="channel">Channel</label>
          <input
            type="text"
            id="channel"
            name="channel"
            onChange={formik.handleChange}
            value={formik.values.channel}
          />
          // Hiển thị thông báo validate tại đây !
          {formik.errors.channel ? (
            <div className="error">{formik.errors.channel}</div>
          ) : null}
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default YoutubeForm;

```

---

### 04. Visited Fields & Improving validation UX (Cải thiện UX xác thực)

```php
     <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
          />
          {formik.errors.name && formik.touched.name ? (   // sử  dụng thêm formik.touched.name để khi người dùng "PHẢI CÓ TÁC ĐỘNG" (click, nhập) vào ô input để nhập thì mới hiện lỗi validate
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>
```

### 05. Schema validation with Yup (Sử dụng yup trong validate form)

Sử dụng như sau :
`yarn add yup` or `npm i yup`

- import

```php
import * as Yup from "yup";
```

- sử dụng :

```php
// hàm thực hiện validation form sử  dụng yup
// created by vatuan 15/09/2020
const validationSchema = Yup.object({
  name: Yup.string().required("Required !"),
  email: Yup.string().email("Invalid email format").required("Required !"),
  channel: Yup.string().required("Required !"),
});

// Using :

  const formik = useFormik({
    initialValues,
    onSubmit,
    // validate,
    validationSchema,  // cho vào trong useFormik là ok !!
  });
```

---

### 06. Reducing boilerplate

**Hiểu nôm na là có thể rút gọn code**

- `Ban đầu :`

```php
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
          />
          {formik.errors.name && formik.touched.name ? (
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>
```

- `Thay đổi :`

```php
 {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            {...formik.getFieldProps("name")}
          />
          {formik.errors.name && formik.touched.name ? (
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>
```

> Như vậy từ 3 dòng code có thể chuyển về 1 dòng bằng cách sử dụng `{...formik.getFieldProps("tên_trường")}`, kết quả vẫn như vậy, vẫn cải thiện UX, vẫn lấy được data submission

---

### 07. Formik, Form, Field, ErrorMessage Components

```php
import React from "react";
import { useFormik, Formik, Form, Field, ErrorMessage } from "formik"; // import để  sử  dụng
import * as Yup from "yup";
// Tạo giá trị khởi tạo ban đầu cho input
const initialValues = {
  name: "",
  email: "",
  channel: "",
};

// hàm thực hiện submit form
// created by vatuan 15/09/2020
const onSubmit = (values) => {
  console.log("Form data", values);
};

// hàm thực hiện validation form sử  dụng yup
// created by vatuan 15/09/2020
const validationSchema = Yup.object({
  name: Yup.string().required("Required !"),
  email: Yup.string().email("Invalid email format").required("Required !"),
  channel: Yup.string().required("Required !"),
});

function YoutubeForm() {
  return (
    <Formik    // 1 thẻ Formik bọc ngoài cùng
      initialValues={initialValues}  // nó nhận các props
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      <Form>   // thẻ <Form /> thay cho <form />
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <Field type="text" id="name" name="name" placeholder="your name"/>  // thẻ <Field /> thay cho <input />
          <ErrorMessage name="name" />  // hiển thị phần error validation
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="email">E-mail</label>
          <Field type="email" id="email" name="email" />
          <ErrorMessage name="email" />
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="channel">Channel</label>
          <Field type="text" id="channel" name="channel" />
          <ErrorMessage name="channel" />
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
}

export default YoutubeForm;

```

---

### 08. Mở rộng hơn với Field và hiển thị error message

```php
import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import TextError from "./TextError";
// Tạo giá trị khởi tạo ban đầu cho input
const initialValues = {
  name: "",
  email: "",
  channel: "",
  comments: "",
  address: "",
};

// hàm thực hiện submit form
// created by vatuan 15/09/2020
const onSubmit = (values) => {
  console.log("Form data", values);
};

// hàm thực hiện validation form sử  dụng yup
// created by vatuan 15/09/2020
const validationSchema = Yup.object({
  name: Yup.string().required("Required !"),
  email: Yup.string().email("Invalid email format").required("Required !"),
  channel: Yup.string().required("Required !"),
  address: Yup.string().required("Required !"),
});

function YoutubeForm() {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
    >
      <Form>
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <Field type="text" id="name" name="name" placeholder="your name" />

          // Hiển thị error message bằng component={TextError}
          <ErrorMessage name="name" component={TextError} />
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="email">E-mail</label>
          <Field type="email" id="email" name="email" />

          // Hiển thị error message bằng cách viết trực tiếp
          <ErrorMessage name="email">
            {(errorMsg) => <div className="error">{errorMsg}</div>}
          </ErrorMessage>
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="channel">Channel</label>
          <Field type="text" id="channel" name="channel" />
          <ErrorMessage name="channel" component={TextError} />
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="comments">Comments</label>
          <Field as="textarea" id="comments" name="comments" />
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="address">Address</label>

          // Mở rộng hơn với Field
          // các props field, form , meta có thể  sử  dụng để  làm nhiều thứ hơn !
          <Field name="address">
            {(props) => {
              const { field, form, meta } = props;
              console.log({ props });
              return (
                <div>
                  <input type="text" id="address" {...field} />
                  {meta.touched && meta.error ? (
                    <div className="error">{meta.error}</div>
                  ) : null}
                </div>
              );
            }}
          </Field>
        </div>

        <button type="submit">Submit</button>
      </Form>
    </Formik>
  );
}

export default YoutubeForm;

```

---

### 09. Nested Objects

- Tạo `nested Object`

```php
const initialValues = {
  name: "",
  email: "",
  channel: "",
  comments: "",
  address: "",

  // Đây chính là nested objects
  social: {
    facebook: "",
    twitter: "",
  },
};
```

- Sử dụng nó :

```php
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="facebook">Facebook</label>
          <Field type="text" id="facebook" name="social.facebook" />  // name này là phải đúng như trong initialValues
        </div>

         {/* form-control */}
        <div className="form-control">
          <label htmlFor="twitter">Twitter</label>
          <Field type="text" id="twitter" name="social.twitter" />
        </div>
```

---

### 09. How to use Arrays and FieldArray

**Cách sử dụng arrays làm initialValues**

- `Tạo array trong initialValues :`

```php
const initialValues = {

 //created to use arrays
 // gía trị khởi tạo ban đầu là array
 phoneNumbers: ["", ""],

};
```

- `Sử dụng : `

```php
      {/* ============ Example for arrays ===========*/}
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="primaryPhone">Primary phone number</label>
          <Field type="text" id="primaryPhone" name="phoneNumbers[0]" />
          // name này phải đặt giống vd : `phoneNumbers` là phải đặt giống
          // name="phoneNumbers[0]" ứng với phần tử  đầu tiên trong mảng
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="secondaryPhone">Secondart phone number</label>
          <Field type="text" id="secondaryPhone" name="phoneNumbers[1]" />
        </div>
```

**Cách sử dụng FieldArray**

- Thường được sử dụng trong trường hợp, nếu muốn nhập nhiều gía trị cho 1 trường, ví dụ nhập nhiều số điện thoại v...v, Xem ví dụ để biết thêm chi tiết

* `Khai báo và khởi tạo`

```php
import {FieldArray} from "formik";  // để  sử   dụng FieldArray

  //created to use FieldArray
  // tạo initialValues như thế  này để có thể  sử  dụng được 'FieldArray'
phNumbers: [""],
```

- `Sử dụng: `

```php
{/* Example for FieldArray */}
        <div className="form-control">
          <label>List of phone numbers</label>
          <FieldArray name="phNumbers">
            {(fieldArrayProps) => {
              // console.log("fieldArrayprops", fieldArrayProps);     //=> một loạt các props mà ta có thể sử  dụng

              // Lấy ra 3 props để sử  dụng trong ví dụ này :
              const { push, remove, form } = fieldArrayProps;
              const { values } = form;
              const { phNumbers } = values;
              // console.log({ phNumbers });    // =>  phNumbers : ['']   __ Kết quả là 1 mảng rỗng

              return (
                <div>
                  {phNumbers.map((phNumber, index) => (
                    <div key={index}>
                      <Field name={`phNumbers[${index}]`} />
                      //  => sau mỗi lần map thì kết qủa kiểu ntn :    <Field name="phNumbers[index]" />  __ index có thể  là 0,1,2...v.v
                      {index > 0 && (
                        <button onClick={() => remove(index)}> - </button>
                      )}
                      <button onClick={() => push("")}> + </button>
                    </div>
                  ))}
                </div>
              );
            }}
          </FieldArray>
        </div>
```

---

### 10. When dose validation run ?

Tùy thuộc vào từng điều kiện, hoàn cảnh mà ta sẽ hiển thị validation, Formik hỗ trợ rất tốt cho việc này
ta có thể có các lựa chọn sau :

```php
  <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      validateOnChange={true}  //thực hiện validate khi có sự thay đổi ô input
      validateOnBlur={true}    // thực hiện validate sau khi rời khỏi ô input hay không , thường để mặc định là 'true'
      validateOnMount={false} // thực hiện validate ngay khi component được Muont
    >
     // something code in here...
  </Formik>
```

---

### 11.Manually trigering validation

có thể sử dụng formik như sau, để lấy được những props của `Formik`
Dùng trong những trường hợp disable button submmit, v..v

```php
 <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      validateOnChange={true} // thực hiện validate khi có sự thay đổi ô input
      validateOnBlur={true} // thực hiện validate sau khi rời khỏi ô input hay không , thường để mặc định là 'true'
      // validateOnMount={true} // thực hiện validate ngay khi component được Muont
    >
      {(formik) => {
        console.log("Formik props", formik);
        return (
          <Form>

          // some code such as :
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="name">Name</label>
              <Field type="text" id="name" name="name" placeholder="your name" />

              // Hiển thị error message bằng component={TextError}
              <ErrorMessage name="name" component={TextError} />
            </div>

             // Ví dụ sử  dụng 'formik props'
            <button
              type="button"
              onClick={() => formik.validateField("comments")}    // validateField là 1 pormik props
            >
              Validate comments
            </button>
            <button type="button" onClick={() => formik.validateForm()}>
              Validate all
            </button>

            <button
              type="button"
              onClick={() => formik.setFieldTouched("comments")}
            >
              Visited Commnents
            </button>

            <button
              type="button"
              onClick={() =>
                formik.setTouched({     // setTouched là 1 formik props
                  name: true,
                  email: true,
                  channel: true,
                  comments: true,
                })
              }
            >
              Visited fields
            </button>
            <button type="submit">Submit</button>
          </Form>
        );
      }}
    </Formik>
```

---

### 12.Load Saved Data

- Ví dụ này được hiểu như sau : bạn có 1 loạt các trường dữ liệu của người dùng được lấy từ sever, bạn muốn hiện hiện các trường đó vào ô input tương ứng, `Formik` sẽ giúp b làm điều này

**Fake : data được lấy từ sever có dạng như thế này :**

```php
// Ví dụ cho việc Load Saved Data , đây là fake data
const savedValues = {
  name: "Vũ Anh Tuấn",
  email: "v@example.com",
  channel: "Tuấn dev",
  comments: "welcome to load save data",
  address: "Tôn Đức Thắng, Đống Đa, Hà Nội",
  social: {
    facebook: "",
    twitter: "",
  },
  phoneNumbers: ["", ""],
  phNumbers: [""],
};
```

**Tại thẻ Formik**

```php
 <Formik
      initialValues={formValues || initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      enableReinitialize // ==>  Đây là 1 props quan trọng để  thực hiện việc "load save data"
    >

     // some code here...
            <button type="button" onClick={() => setFormValues(savedValues)}>
              Load saved data  //  khi click thì những trường trong savedValues sẽ hiển thị vào các ô input tương ứng
            </button>


            <button type="submit">Submit</button>
    </Formik>
```

---

### 13.Reset Form Data

**TH1: Khi người dùng đang nhập liệu, muốn trở về trạng thái ban đầu**

```php
            //  khi click thì những ô được nhập sẽ bị clear trở về  initialVlues
            // nhớ là chỉ clear về  initialValues
            <button type="reset">Reset</button>
```

**TH2 : Khi người dùng submit, thì form sẽ về trạng thái initialValues**

```php
const onSubmit = (values, onSubmitProps) => {
  console.log("Form data", values);

  console.log("submit props", onSubmitProps);

  onSubmitProps.setSubmitting(false);
  onSubmitProps.resetForm();
};

<Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
    >
</Formik>
```

## --> 13 topic trên là cách sử dụng Formik cơ bản, xoay quanh việc validate form, display error, submit form và cách sử dụng khác nhau với initialVlaues

=======================================================================

## Phần tiếp theo sẽ là cách tổ chức và dùng lại (Reusable) Formik

## Building Reusable Formik Controls

---
