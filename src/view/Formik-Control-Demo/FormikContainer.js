import React, { useState } from "react";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormikControl from "./components/FormikControl";

function FormikContainer() {
  const initialValues = {
    email: "",
    password: "",
    description: "",
    selectOption: "",
    radioOption: "Nam",
    checkboxOption: [],
    birthDate: null,
  };
  const validationSchema = Yup.object({
    email: Yup.string()
      .email("Email is not valid !")
      .required("Email is Required !"),
    password: Yup.string().required("Password is requied !"),
    description: Yup.string().required("Description is requied !"),
    selectOption: Yup.string().required("select is requied !"),
    radioOption: Yup.string().required("Requied !"),
    checkboxOption: Yup.array().required("Required !"),
    birthDate: Yup.date().required("Requied ! ").nullable(),
  });
  const onSubmit = (values, onSubmitprops) => {
    console.log("Form data", values);
    console.log("Submit props", onSubmitprops);
    onSubmitprops.resetForm();
  };

  const dropdownOptions = [
    { key: "select an option", value: "" },
    { key: "Option 1", value: "option 1" },
    { key: "Option 2", value: "option 2" },
    { key: "Option 3", value: "option 3" },
  ];

  const radioOptions = [
    { key: "Nam", value: "Nam" },
    { key: "Nữ", value: "Nữ" },
    { key: "Xăng pha nhớt", value: "Xăng pha nhớt" },
  ];

  const checkboxOptions = [
    { key: "option 1", value: "cOption1" },
    { key: "option 2", value: "cOption2" },
    { key: "option 3", value: "cOption3" },
  ];
  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {(formik) => (
          <Form>
            {/* Example input */}
            <FormikControl
              control="input"
              type="email"
              label="Email"
              name="email"
            />
            {/* Example input password  */}
            <FormikControl
              control="input"
              type="password"
              label="Password"
              name="password"
            />
            {/* Exmaple textarea */}
            <FormikControl
              control="textarea"
              label="Description"
              name="description"
            />
            {/* Example select */}
            <FormikControl
              control="select"
              label="Select a topic"
              name="selectOption"
              options={dropdownOptions}
            />
            {/* Example radio buttons */}{" "}
            <FormikControl
              control="radio"
              label="Gender"
              name="radioOption"
              options={radioOptions}
            />
            {/* Example checkbox form  */}
            <FormikControl
              control="checkbox"
              label="Checkbox topics"
              name="checkboxOption"
              options={checkboxOptions}
            />
            {/* Example date picker */}
            <FormikControl
              control="date"
              label="Pick a date"
              name="birthDate"
            />
            <button type="submit">Submit</button>
          </Form>
        )}
      </Formik>
    </div>
  );
}

export default FormikContainer;
