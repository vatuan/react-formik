import React from "react";
import "./App.scss";
import YoutubeForm from "./components/YoutubeForm";
import OldYoutubeForm from "./components/OldYoutubeForm";
import FormikContainer from "./view/Formik-Control-Demo/FormikContainer";
import LoginForm from "./view/Formik-Control-Demo/components/LoginForm";

function App() {
  return (
    <div className="App">
      {/* <OldYoutubeForm /> */}
      {/* <YoutubeForm /> */}
      {/* <FormikContainer /> */}

      <LoginForm />
    </div>
  );
}

export default App;
