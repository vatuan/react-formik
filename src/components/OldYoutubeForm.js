import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
// Tạo giá trị khởi tạo ban đầu cho input
const initialValues = {
  name: "Vu Anh Tuan",
  email: "",
  channel: "",
};

// hàm thực hiện submit form
// created by vatuan 15/09/2020
const onSubmit = (values) => {
  console.log("Form data", values);
};

// hàm thực hiện validate form
// created by vatuan 15/09/2020
const validate = (values) => {
  // values.name  values.email values.channel
  // errors.name errors.email errors.channel
  // errors.name = 'This field is required'
  // điều kiện đầu tiên của validate form là phải tạo ra 1 object sau đó return về object đó, trong ví dụ, object này chính là 'errors'
  let errors = {};
  if (!values.name) {
    errors.name = "Required";
  }
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = "Invalid email format";
  }
  if (!values.channel) {
    errors.channel = "Required";
  }
  return errors;
};

// hàm thực hiện validation form sử  dụng yup
// created by vatuan 15/09/2020
const validationSchema = Yup.object({
  name: Yup.string().required("Required !"),
  email: Yup.string().email("Invalid email format").required("Required !"),
  channel: Yup.string().required("Required !"),
});

function OldYoutubeForm() {
  const formik = useFormik({
    initialValues,
    onSubmit,
    validate, // trong ví dụ này mới bắt đầu tìm hiểu
    // validationSchema,
  });
  // console.log("Form value", formik.values);
  // => Form value {name: "", email: "", channel: ""}

  // console.log("Form eror : ", formik.errors);
  // =>Form eror : {email: "Required", channel: "Required"}
  // Như vậy có thế lấy ra giá error-message bằng cách : formik.errors.email , kết quả trả về  sẽ là : 'Required'

  console.log("Visited Fields", formik.touched);
  // trả về  kiểu bollean xem bạn đã click, hay nhập vào ô input đó hay chưa
  // vi formik.touched.name === true , thì b đã nhập hoặc click vào ô input đó rồi
  return (
    <div>
      <form onSubmit={formik.handleSubmit}>
        {/* form-control */}
        <div className="form-control">
          <label htmlFor="name">Name</label>
          <input
            type="text"
            id="name"
            name="name"
            onChange={formik.handleChange}
            value={formik.values.name}
            onBlur={formik.handleBlur}
          />
          {formik.errors.name && formik.touched.name ? (
            <div className="error">{formik.errors.name}</div>
          ) : null}
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="email">E-mail</label>
          <input
            type="email"
            id="email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          {formik.errors.email && formik.touched.email ? (
            <div className="error">{formik.errors.email}</div>
          ) : null}
        </div>

        {/* form-control */}
        <div className="form-control">
          <label htmlFor="channel">Channel</label>
          <input
            type="text"
            id="channel"
            name="channel"
            onChange={formik.handleChange}
            value={formik.values.channel}
            onBlur={formik.handleBlur}
          />
          {formik.errors.channel && formik.touched.channel ? (
            <div className="error">{formik.errors.channel}</div>
          ) : null}
        </div>

        <button type="submit">Submit</button>
      </form>
    </div>
  );
}

export default OldYoutubeForm;
