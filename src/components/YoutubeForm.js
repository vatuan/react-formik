import React, { useState } from "react";
import {
  Formik,
  Form,
  Field,
  ErrorMessage,
  FieldArray,
  FastField,
} from "formik";
import * as Yup from "yup";
import TextError from "./TextError";

// Tạo giá trị khởi tạo ban đầu cho input
const initialValues = {
  name: "Tuấn",
  email: "",
  channel: "",
  comments: "",
  address: "",
  // nested objects
  social: {
    facebook: "",
    twitter: "",
  },

  //created to use arrays
  phoneNumbers: ["", ""],

  //created to use FieldArray
  phNumbers: [""],
};

// hàm thực hiện submit form
// created by vatuan 15/09/2020
const onSubmit = (values, onSubmitProps) => {
  console.log("Form data", values);

  console.log("submit props", onSubmitProps);

  onSubmitProps.setSubmitting(false);
  onSubmitProps.resetForm();
};

// hàm thực hiện validation form sử  dụng yup
// created by vatuan 15/09/2020
const validationSchema = Yup.object({
  name: Yup.string().required("Required !"),
  email: Yup.string().email("Invalid email format").required("Required !"),
  channel: Yup.string().required("Required !"),
  address: Yup.string().required("Required !"),
  social: Yup.object().shape({
    facebook: Yup.string().required("required !"), //  đây là ví dụ cho validation nested object
  }),
});

// hàm thưc hiện validation trường Comments
// created by vatuan 15/09/2020
// ví dụ này là tìm hiểu thêm về  cách validation khác thôi nhé !
const validateComments = (value) => {
  let error;
  if (!value) {
    error = "Comment đi, không được để trống ";
  }
  return error;
};

// Ví dụ cho việc Load Saved Data , đây là fake data
const savedValues = {
  name: "Vũ Anh Tuấn",
  email: "v@example.com",
  channel: "Tuấn dev",
  comments: "welcome to load save data",
  address: "Tôn Đức Thắng, Đống Đa, Hà Nội",
  // nested objects
  social: {
    facebook: "",
    twitter: "",
  },

  //created to use arrays
  phoneNumbers: ["", ""],

  //created to use FieldArray
  phNumbers: [""],	
};
function YoutubeForm() {
  const [formValues, setFormValues] = useState(null);
  return (
    <Formik
      initialValues={formValues || initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      validateOnChange={true} // thực hiện validate khi có sự thay đổi ô input
      validateOnBlur={true} // thực hiện validate sau khi rời khỏi ô input hay không , thường để mặc định là 'true'
      // validateOnMount={true} // thực hiện validate ngay khi component được Muont

      enableReinitialize // Đây là 1 props quan trọng để  xét
    >
      {(formik) => {
        console.log("Formik props", formik);
        return (
          <Form>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="name">Name</label>
              <FastField
                type="text"
                id="name"
                name="name"
                placeholder="your name"
              />

              {/* Thực hiện hiển thị error message (validate) bằng component={TextError}  */}
              <ErrorMessage name="name" component={TextError} />
            </div>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="email">E-mail</label>
              <FastField type="email" id="email" name="email" />

              {/* Hiển thị error message (validate) bằng cách viết trực tiếp , không thông qua component TextError*/}
              <ErrorMessage name="email">
                {(errorMsg) => <div className="error">{errorMsg}</div>}
              </ErrorMessage>
            </div>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="channel">Channel</label>
              <FastField type="text" id="channel" name="channel" />
              <ErrorMessage name="channel" component={TextError} />
            </div>
            {/* form-control */}
            {/* Đây là ví dụ cho một cách validate khác  */}
            <div className="form-control">
              <label htmlFor="comments">Comments</label>
              <Field
                as="textarea"
                id="comments"
                name="comments"
                validate={validateComments} // không validate bằng 'yup'
              />
              <ErrorMessage name="comments" component={TextError} />
            </div>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="address">Address</label>

              {/*  Mở rộng hơn với Field */}
              <FastField name="address">
                {(props) => {
                  // console.log("Field render");
                  const { field, form, meta } = props;
                  return (
                    <div>
                      <input type="text" id="address" {...field} />
                      {meta.touched && meta.error ? (
                        <div className="error">{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
              </FastField>
            </div>
            {/* ================ Example for nested objects ================== */}
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="facebook">Facebook</label>
              <Field type="text" id="facebook" name="social.facebook" />
              <ErrorMessage name="social.facebook" component={TextError} />
            </div>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="twitter">Twitter</label>
              <Field type="text" id="twitter" name="social.twitter" />
            </div>
            {/* ============ Example for arrays ===========*/}
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="primaryPhone">Primary phone number</label>
              <Field type="text" id="primaryPhone" name="phoneNumbers[0]" />
            </div>
            {/* form-control */}
            <div className="form-control">
              <label htmlFor="secondaryPhone">Secondart phone number</label>
              <Field type="text" id="secondaryPhone" name="phoneNumbers[1]" />
            </div>
            {/* Example for FieldArray */}
            <div className="form-control">
              <label>List of phone numbers</label>
              <FieldArray name="phNumbers">
                {(fieldArrayProps) => {
                  // console.log("fieldArrayprops", fieldArrayProps);
                  const { push, remove, form } = fieldArrayProps;
                  const { values } = form;
                  const { phNumbers } = values;
                  // console.log({ phNumbers });

                  return (
                    <div>
                      {phNumbers.map((phNumber, index) => (
                        <div key={index}>
                          <Field name={`phNumbers[${index}]`} />
                          {index > 0 && (
                            <button onClick={() => remove(index)}> - </button>
                          )}
                          <button onClick={() => push("")}> + </button>
                        </div>
                      ))}
                    </div>
                  );
                }}
              </FieldArray>
            </div>
            {/* <button
              type="button"
              onClick={() => formik.validateField("comments")}
            >
              Validate comments
            </button>
            <button type="button" onClick={() => formik.validateForm()}>
              Validate all
            </button>

            <button
              type="button"
              onClick={() => formik.setFieldTouched("comments")}
            >
              Visited Commnents
            </button>

            <button
              type="button"
              onClick={() =>
                formik.setTouched({
                  name: true,
                  email: true,
                  channel: true,
                  comments: true,
                })
              }
            >
              Visited fields
            </button> */}
            <button type="button" onClick={() => setFormValues(savedValues)}>
              Load saved data
            </button>

            {/*  khi click thì những ô được nhập sẽ bị clear trở về initialVlues */}
            <button type="reset">Reset</button>

            <button type="submit">Submit</button>
          </Form>
        );
      }}
    </Formik>
  );
}

export default YoutubeForm;
